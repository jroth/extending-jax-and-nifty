Extending NIFTy and Jax with C++ code
==========================================

This repository contains code demonstrating how to extend NIFTy and Jax with custom C++ code. In the NIFTy example custom C++ code, computing the square of the input, is wrapped as a nifty operator. The Jax example exposes the hartly transformation form  [ducc](https://gitlab.mpcdf.mpg.de/mtr/ducc) as a JAX primitive. The Jax example builds on the demo from [extending-jax](https://github.com/dfm/extending-jax). The [extending-jax](https://github.com/dfm/extending-jax) repository additionally includes instructions how to bind Cuda code to jax for GPU computing. Further information on how to bind C++/ Cuda code to Jax can be found in the jax documentation [here](https://jax.readthedocs.io/en/latest/notebooks/How_JAX_primitives_work.html) and [here](https://jax.readthedocs.io/en/latest/Custom_Operation_for_GPUs.html).



Installation
------------

To execute the example clone the repository and compile the C++ code.

    git clone --recursive https://gitlab.mpcdf.mpg.de/jroth/extending-jax-and-nifty.git
    cd extending-jax-and-nifty
    make
