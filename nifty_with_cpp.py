import numpy as np
import nifty8 as ift


class Pybind11Operator(ift.Operator):
    def __init__(self, domain, target, pb11_op):
        self._domain  = ift.DomainTuple.make(domain)
        self._target = ift.DomainTuple.make(target)
        self._op = pb11_op()

    def apply(self, x):
        self._check_input(x)
        if ift.is_linearization(x):
            lin = self._op.apply_with_jac(x.val.val)
            jac = Pybind11LinearOperator(self._domain, self._target, lin.jac_times, lin.jac_adjoint_times)
            pos = ift.makeField(self._target, lin.position())
            return x.new(pos, jac)
        return ift.makeField(self.target, self._op.apply(x.val))


class Pybind11LinearOperator(ift.LinearOperator):
    def __init__(self, domain, target, times, adj_times):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(target)
        self._capability = self.TIMES | self.ADJOINT_TIMES
        self._times = times
        self._adj_times = adj_times

    def apply(self, x, mode):
        self._check_input(x, mode)
        res = (self._times if mode == self.TIMES else self._adj_times)(x.val)
        return ift.makeField(self._tgt(mode), res)


# init op
from cpp_for_nifty import SquareOperator as sq_op_cpp
dom = ift.UnstructuredDomain([10, 12])
sq_op = Pybind11Operator(dom, dom, sq_op_cpp)

# test op
loc = ift.from_random(sq_op.domain)
res = sq_op.apply(loc)
np.testing.assert_allclose((loc*loc).val, res.val)
ift.extra.check_operator(sq_op, loc)
