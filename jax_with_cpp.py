from functools import partial

import numpy as np
from jax import core, dtypes, lax
from jax import numpy as jnp
from jax.core import ShapedArray
from jax.interpreters import ad, batching, mlir, xla
from jax.lib import xla_client
from jaxlib.hlo_helpers import custom_call

import cpp_for_jax

for _name, _value in cpp_for_jax.registrations().items():
    xla_client.register_custom_call_target(_name, _value, platform="cpu")

# function exposed to the user. The rest is wrapper code
def hartly(x):
    return _hartly_prim.bind(x)[0]



def _hartly_abstract(x):
    shape = x.shape
    dtype = dtypes.canonicalize_dtype(x.dtype)
    return (ShapedArray(shape, dtype), )


def _hartly_lowering(ctx, x, *, platform="cpu"):
    x_aval = ctx.avals_in[0]
    np_dtype = np.dtype(x_aval.dtype)
    dtype = mlir.ir.RankedTensorType(x.type)
    dims = dtype.shape
    layout = tuple(range(len(dims) - 1, -1, -1))

    assert len(dims) == 2 # only for 2d 
    size0 = np.array(dims[0]).astype(np.int64)
    size1 = np.array(dims[1]).astype(np.int64)

    if np_dtype == np.float32:
        op_name = platform + "_hartly_f32"
    elif np_dtype == np.float64:
        op_name = platform + "_hartly_f64"
    else:
        raise NotImplementedError(f"Unsupported dtype {np_dtype}")

    if platform == "cpu":
        return custom_call(
            op_name,
            result_types=[dtype, ],
            operands=[mlir.ir_constant(size0), mlir.ir_constant(size1),  x],
            operand_layouts=[(), (),  layout],
            result_layouts=[layout, ]
        ).results

    elif platform == "gpu":
        raise ValueError(
            "No GPU support"
        )
    raise ValueError(
        "Unsupported platform; this must be either 'cpu' or 'gpu'"
    )


def _hartly_jvp(args, tangents):
    x = args[0]
    dx = tangents[0]
    res = _hartly_prim.bind(x)

    if type(dx) is ad.Zero:
        return (res, lax.zeros_like_array(x))
    else:
        return (res, _hartly_prim.bind(dx))

def _hartly_transpose(cotangents, args):
    res = _hartly_prim.bind(cotangents[0])
    return (res, )


def _hartly_batch(args, axes):
    raise NotImplementedError("FIXME")
    return hartly(*args), axes


_hartly_prim = core.Primitive("hartly")
_hartly_prim.multiple_results = True
_hartly_prim.def_impl(partial(xla.apply_primitive, _hartly_prim))
_hartly_prim.def_abstract_eval(_hartly_abstract)


for platform in ["cpu", "gpu"]:
    mlir.register_lowering(
        _hartly_prim,
        partial(_hartly_lowering, platform=platform),
        platform=platform)

ad.primitive_jvps[_hartly_prim] = _hartly_jvp
ad.primitive_transposes[_hartly_prim] = _hartly_transpose
batching.primitive_batchers[_hartly_prim] = _hartly_batch



################## TEST ##################

a = np.full((10, 10), 1.)
from jax import jvp
print(jvp(hartly, [a], [a]))

from jax import vjp
pr, hartly_vjp = vjp(hartly, a)
print(hartly_vjp(a))

