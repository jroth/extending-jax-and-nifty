#include <pybind11/pybind11.h>

#include <cmath>

#include "ducc0/bindings/pybind_utils.h"
#include "ducc0/fft/fft.h"
#include "ducc0/fft/fftnd_impl.h"


// https://en.cppreference.com/w/cpp/numeric/bit_cast
template <class To, class From>
typename std::enable_if<sizeof(To) == sizeof(From) && std::is_trivially_copyable<From>::value &&
                            std::is_trivially_copyable<To>::value,
                        To>::type
bit_cast(const From& src) noexcept {
  static_assert(
      std::is_trivially_constructible<To>::value,
      "This implementation additionally requires destination type to be trivially constructible");

  To dst;
  memcpy(&dst, &src, sizeof(To));
  return dst;
}

template <typename T>
std::string PackDescriptorAsString(const T& descriptor) {
  return std::string(bit_cast<const char*>(&descriptor), sizeof(T));
}

template <typename T>
const T* UnpackDescriptor(const char* opaque, std::size_t opaque_len) {
  if (opaque_len != sizeof(T)) {
    throw std::runtime_error("Invalid opaque object size");
  }
  return bit_cast<const T*>(opaque);
}



template <typename T>
pybind11::bytes PackDescriptor(const T& descriptor) {
  return pybind11::bytes(PackDescriptorAsString(descriptor));
}

template <typename T>
pybind11::capsule EncapsulateFunction(T* fn) {
  return pybind11::capsule(bit_cast<void*>(fn), "xla._CUSTOM_CALL_TARGET");
}


template <typename T>
void hartly(void *out_tuple, const void **in) {
  // Parse the inputs
  const std::int64_t size0 = *reinterpret_cast<const std::int64_t *>(in[0]);
  const std::int64_t size1 = *reinterpret_cast<const std::int64_t *>(in[1]);
  const T *jax_arr = reinterpret_cast<const T *>(in[2]);

  ducc0::cfmav<T> fin(jax_arr, {size_t(size0), size_t(size1)});
  const T dx = 1. / T(size0);
  const T dy = 1. / T(size1);
  const T fct = std::sqrt(dx * dy);
  ducc0::vfmav<T> out(reinterpret_cast<T *>(out_tuple), {size_t(size0), size_t(size1)});
  ducc0::r2r_genuine_hartley(fin, out, {0,1}, fct, 1);
}


pybind11::dict Registrations() {
  pybind11::dict dict;
  dict["cpu_hartly_f32"] = EncapsulateFunction(hartly<float>);
  dict["cpu_hartly_f64"] = EncapsulateFunction(hartly<double>);
  return dict;
}

PYBIND11_MODULE(cpp_for_jax, m) { m.def("registrations", &Registrations); }
