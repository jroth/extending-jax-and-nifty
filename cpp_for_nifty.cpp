#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>


using namespace pybind11::literals;
namespace py = pybind11;

using namespace std;


class Linearization {
  public:
    Linearization(const py::array &position_,
                  function<py::array(const py::array &)> jvp_,
                  function<py::array(const py::array &)> vjp_)
        : p(position_), jvp(jvp_), vjp(vjp_) {}

    py::array jac_times(const py::array &inp) const { return jvp(inp); }
    py::array jac_adjoint_times(const py::array &inp) const { return vjp(inp); }
    py::array position() { return p; };

  private:
    const py::array p;
    const function<py::array(const py::array &)> jvp;
    const function<py::array(const py::array &)> vjp;
};

class SquareOperator {
  public:
    SquareOperator() {}
    py::array apply(const py::array &inp) const {
        auto pyout = inp * inp;
        return pyout;
    }

    Linearization apply_with_jac(const py::array &d) {
        function<py::array(const py::array &)> f = [=](const py::array &inp) {
            auto tmp = d * inp;
            return tmp+tmp;
        };

        function<py::array(const py::array &)> jvp =
            [=](const py::array &inp) { return f(inp); };
        function<py::array(const py::array &)> vjp =
            [=](const py::array &inp) { return f(inp); };

        return Linearization(apply(d), jvp, vjp);
    }
};



PYBIND11_MODULE(cpp_for_nifty, m) {
    py::class_<SquareOperator>(m, "SquareOperator")
        .def(py::init<>())
        .def("apply", &SquareOperator::apply)
        .def("apply_with_jac", &SquareOperator::apply_with_jac);

    py::class_<Linearization>(m, "Linearization")
        .def(py::init<const py::array &, function<py::array(const py::array &)>,
                      function<py::array(const py::array &)>>())
        .def("position", &Linearization::position)
        .def("jac_times", &Linearization::jac_times)
        .def("jac_adjoint_times", &Linearization::jac_adjoint_times);
}
