.PHONY: all clean

FLAGS_DUCC = -Iducc/src -pthread
FLAGS_MINI:= -std=c++20 -Wfatal-errors -Wall -Wextra -shared -fPIC $(shell python3 -m pybind11 --includes)  -fvisibility=hidden
FLAGS:= -O3 -march=native -ffast-math $(FLAGS_MINI)
FLAGS_MINI_DUCC:= $(FLAGS_MINI) -Iducc/src 
FLAGS_DUCC:= $(FLAGS) -Iducc/src

OUT_JAX:= cpp_for_jax$(shell python3-config --extension-suffix)
OUT_NIFTY:= cpp_for_nifty$(shell python3-config --extension-suffix)

all: $(OUT_JAX) $(OUT_NIFTY)

$(OUT_JAX) : cpp_for_jax.cpp
	g++ $(FLAGS_DUCC) -o$(OUT_JAX) ducc/src/ducc0/infra/mav.cc ducc/src/ducc0/infra/threading.cc ducc/src/ducc0/infra/string_utils.cc $^

$(OUT_NIFTY) : cpp_for_nifty.cpp
	g++ $(FLAGS) -o$(OUT_NIFTY) $^

clean:
	rm $(OUT_JAX)
	rm $(OUT_NIFTY)
